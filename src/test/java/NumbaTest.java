import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;


class NumbaTest {

    @Test
    void add() {
        Numba n = new Numba();
        assertEquals(n.add(3,4), 7);
        assertNotEquals(n.add(3,5), 7);
    }
}